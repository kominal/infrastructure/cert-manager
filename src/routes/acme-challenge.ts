import { Router } from '@kominal/lib-node-express-router';
import { info, warn } from '@kominal/observer-node-client';
import { ongoingChallenges } from '../acme-manager';

export const acmeChallengeRouter = new Router();

acmeChallengeRouter.getAsGuest<{ token: string }, string, never>('/.well-known/acme-challenge/:token', async (req) => {
	const token = req.params.token;

	const challenge = ongoingChallenges.find((c) => c.token === token);

	if (challenge) {
		info(`Answering challenge with token ${token} and key ${challenge.keyAuthorization}.`);
		return {
			statusCode: 200,
			responseBody: challenge.keyAuthorization,
		};
	}

	warn(`Could not answer challenge with token ${token}...`);
	return { statusCode: 404 };
});
