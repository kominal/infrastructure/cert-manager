import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { error, info, startObserver } from '@kominal/observer-node-client';
import { exit } from 'process';
import { acmeChallengeRouter } from './routes/acme-challenge';
import { CertificatesScheduler } from './scheduler/certificates.scheduler';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start() {
	try {
		startObserver();
		mongoDBInterface = new MongoDBInterface('certificates');
		await mongoDBInterface.connect();
		expressRouter = new ExpressRouter({
			healthCheck: async () => true,
			routes: [acmeChallengeRouter],
		});
		await expressRouter.start();
		await new CertificatesScheduler().start(3600);
	} catch (e) {
		error(`Failed to start service: ${e}`);
		exit(1);
	}
}
start();

process.on('SIGTERM', async () => {
	info(`Received system signal 'SIGTERM'. Shutting down service...`);
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
});
