import { error } from '@kominal/observer-node-client';
import acme, { Client } from 'acme-client';
import { PropertyDatabase } from './models/property';

export type TraefikChallange = { timestamp: number; token: string; keyAuthorization: string };
export type CertificateWrapper = { csr: string; key: string; cert: string };

let client: Client | undefined;

export let ongoingChallenges: TraefikChallange[] = [];

export async function setup() {
	const property = await PropertyDatabase.findOne({ key: 'ACME_ACCOUNT_KEY' });
	const accountKey = property ? Buffer.from(property.value, 'base64') : await acme.forge.createPrivateKey();

	client = new Client({
		directoryUrl: acme.directory.letsencrypt.production,
		accountKey,
	});
	await client.createAccount({
		termsOfServiceAgreed: true,
		contact: ['mailto:mail@kominal.com'],
	});

	if (!property) {
		await PropertyDatabase.updateMany({ key: 'ACME_ACCOUNT_KEY' }, { value: accountKey.toString('base64') }, { upsert: true });
	}
}

export async function requestNewCertificate(domain: string): Promise<CertificateWrapper | null> {
	if (!client) {
		await setup();
	}

	if (!client) {
		error('Client could not be created.');
		return null;
	}

	const timestamp = new Date().getTime();

	try {
		const order = await client.createOrder({
			identifiers: [{ type: 'dns', value: domain }],
		});

		const authorizations = await client.getAuthorizations(order);

		const authorization = authorizations[0];

		if (!authorization) {
			error('Authorization is null.');
			return null;
		}

		const challenge = authorization.challenges.find((c) => c.type === 'http-01');
		if (!challenge) {
			error('Challenge is null.');
			return null;
		}

		const keyAuthorization = await client.getChallengeKeyAuthorization(challenge);

		ongoingChallenges.push({ timestamp, token: challenge.token, keyAuthorization });

		await client.verifyChallenge(authorization, challenge);

		await client.completeChallenge(challenge);

		await client.waitForValidStatus(challenge);

		const [key, csr] = await acme.forge.createCsr({
			commonName: domain,
		});
		await client.finalizeOrder(order, csr);
		const cert = await client.getCertificate(order);

		return {
			csr: csr.toString('base64'),
			key: key.toString('base64'),
			cert: Buffer.from(cert).toString('base64'),
		};
	} catch (error) {
		console.log(error);
	}
	ongoingChallenges = ongoingChallenges.filter((c) => c.timestamp != timestamp);

	return null;
}
